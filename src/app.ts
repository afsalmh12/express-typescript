import { Application } from 'express'
import express from 'express'
import end_points from './routes/routes';
import startServer from './configs/configs';
var app:Application = express();
startServer(app)
end_points(app)

