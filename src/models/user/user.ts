
import mongoose from 'mongoose'
import { IUser } from 'types/interfaces/user-interfaces';
const schema = mongoose.Schema;
//defining user schema
const userSchema = new schema({
    name: String,
    email:String,
    password:String
},{
    versionKey: false 
})
//exporting user schema
const User = mongoose.model<IUser>("user",userSchema,"user");
export default User 
