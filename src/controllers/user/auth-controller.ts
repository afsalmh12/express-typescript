
import { AuthService } from '../../services/user/auth-service'
import { JwtHandler } from '../../utils/classes/jwt-handler'
import { Request, Response } from 'express'

import { IAuth, IToken } from 'types/interfaces/user-interfaces'
import { ControllerBase } from '../../utils/classes/controller-base'


export class AuthController extends ControllerBase {
    private auth_service = new AuthService()
    private jwt_handler = new JwtHandler()
    get(request: Request, response: Response): Promise<any> {
        throw new Error("Method not implemented.")
    }
    put(request: Request, response: Response): Promise<any> {
        throw new Error("Method not implemented.")
    }
    patch(request: Request, response: Response): Promise<any> {
        throw new Error("Method not implemented.")
    }
    delete(request: Request, response: Response): Promise<any> {
        throw new Error("Method not implemented.")
    }

    constructor() {
        super();
    }
    async post(req: Request, res: Response) {
        const request: IAuth = req.body
        if (request.email && request.password) {
            const user = await this.auth_service.login(request.email);
            if (user) {
                const validUser = await this.jwt_handler.verifyHash(user.password, request.password);
                if (validUser) {
                    const token: string = await this.jwt_handler.generateToken(request.email);
                    token ? this.response<IToken>(res, { token: token }) : this.error(res, 500)
                } else {
                    this.error(res, 401)
                }
            } else {
                this.error(res, 401)
            }
        } else {
            this.error(res, 400)
        }

    }

}
