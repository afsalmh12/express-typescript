
import { JwtHandler } from '../../utils/classes/jwt-handler'
import { Request, Response } from 'express'

import { IToken, IUser } from 'types/interfaces/user-interfaces'
import { ControllerBase } from '../../utils/classes/controller-base'
import { UserService } from '../../services/user/user-service'
import { ExpressHttpRequest } from 'types/interfaces/core-interfaces'

export class UserController extends ControllerBase {
    private user_service= new UserService()
    private jwt_handler = new JwtHandler()
    constructor() {
        super();
    }
    async post(request: Request, response: Response) {
        let new_user: IUser = request.body;
        if (new_user && new_user.email && new_user.password) {
            let user = await this.user_service.verifyEmail(new_user.email);
            if (!user) {
                new_user.password = await this.jwt_handler.getPasswordHash(new_user.password);
                this.user_service.addUser(new_user).then((result) => {
                    this.jwt_handler.generateToken(result.email).then((token) => {
                        this.response<IToken>(response, { token });
                    }).catch((err) => {
                        this.error(response, 500)
                    })

                })
            } else {
                this.error(response, 500, "Email-Id already exist")
            }
        } else {
            this.error(response, 400)
        }
    }
    async get(request: Request, response: Response<any>): Promise<any> {

        const user:IUser|null = await this.user_service.profile((request as ExpressHttpRequest)._id)
        this.response<IUser>(response,user)
    }
    put(request: Request, response: Response<any>): Promise<any> {
        throw new Error("Method not implemented.");
    }
    patch(request: Request, response: Response<any>): Promise<any> {
        throw new Error("Method not implemented.");
    }
    delete(request: Request, response: Response<any>): Promise<any> {
        throw new Error("Method not implemented.");
    }
}

