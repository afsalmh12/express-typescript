
 const raiseError = (error_code:number) => {
    let error_message = ''
    switch (error_code){
        case 400 : error_message = "Bad request"
        break;
        case 500 : error_message = "Internal server error"
        break;
        case 401 : error_message = "Unauthorized request"
    }
    return error_message
}
 export default raiseError 
