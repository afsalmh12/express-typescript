import { Request, Response } from 'express'
import raiseError from '../functions/error-handler';

export abstract class ControllerBase {
    abstract async post(request: Request, response: Response): Promise<void | any>
    abstract async get(request: Request, response: Response): Promise<void | any>
    abstract async put(request: Request, response: Response): Promise<void | any>
    abstract async patch(request: Request, response: Response): Promise<void | any>
    abstract async delete(request: Request, response: Response): Promise<void | any>

    public response<T>(response: Response, result?: T|null) {
        if (result) {
            response.type('application/json');
            return response.status(200).json(result);
        } else {
            return response.sendStatus(200);
        }
    }

    public error(response:Response,code:number,message?:string){
        const err = message ? message : raiseError(code)
        response.status(code).send(err);
    }
}