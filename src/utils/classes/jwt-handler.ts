
import error_handler from '../functions/error-handler'
import bcrypt from 'bcrypt'
import jwt, { Secret } from 'jsonwebtoken'
import { Request,Response } from 'express'
import * as dotenv from "dotenv";
dotenv.config();
const saltRounds = 10;

export class JwtHandler {
    constructor() {

    }
    verifyJwtToken(req:any, res:any, next:any) {
        var token;
        //get token from header
        token = req.headers['authorization'];
        if (!token)
            return res.status(403).send(error_handler(401));
        else {
            jwt.verify(token, process.env.JWT_SUPER_KEY as Secret, (err:any, decoded:any) => {
                if (err)
                    return res.status(403).send(error_handler(401));
                else {
                    req._id = decoded.email;
                    next();
                }
            })
        }
    }
    getPasswordHash(password:string):Promise<string> {
        return new Promise((resolve, reject) => {
            bcrypt.hash(password, saltRounds)
                .then((hash) => { resolve(hash); })
                .catch((err) => { reject(err) })
        })
    }
    generateToken(data:string):Promise<string> {
        return new Promise((resolve, reject) => {
            resolve(jwt.sign({ email: data }, process.env.JWT_SUPER_KEY as Secret, {
                expiresIn: 86400 // expires in 1 day
            }))
        })
    }
    //function to verify password
    verifyHash(hash:string, password:string):Promise<boolean> {
        return new Promise(function (resolve, reject) {
            bcrypt.compare(password, hash)
                .then((res) => {
                     resolve(res) 
                    })
                .catch((err) => { 
                    reject(err) 
                })
        });
    }
}
