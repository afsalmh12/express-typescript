import {Router} from 'express'
const router = Router()
import {UserController} from '../../controllers/user/user-controller'
const user = new UserController()

router.get('/profile',(req,res) =>user.get(req,res))
export default router