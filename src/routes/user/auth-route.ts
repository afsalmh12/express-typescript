import {Router, request} from 'express'
const router = Router()
import {AuthController} from '../../controllers/user/auth-controller'
const auth = new AuthController()
import {UserController} from '../../controllers/user/user-controller'

const user = new UserController()
router.post('/login', (req,res) => auth.post(req,res));
router.post('/signup',(req,res) =>user.post(req,res));
export default router