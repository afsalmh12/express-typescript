
import {JwtHandler} from '../utils/classes/jwt-handler'
import { Application } from 'express'
import auth from './user/auth-route';
import user from './user/user-route'
import { format } from 'url';
const jwt = new JwtHandler()
const end_points = (app:Application) => {
    app.use('/auth', auth)
    app.use('/user',jwt.verifyJwtToken,user)
}
export default end_points
