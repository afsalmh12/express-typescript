import bodyParser = require('body-parser')
import cors = require('cors')
import { Application } from 'express'
const middlewares = (app:Application) => {
    app.use(cors());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
}
export default middlewares