import { IAuth, IUser } from "types/interfaces/user-interfaces";

import User from "../../models/user/user"




//function to verify user credentials 
export class UserService {
    constructor() { }
    login(emailId: string): Promise<IUser|null> {
        return new Promise(function (resolve, reject) {
            User.findOne({ email: emailId }, 'email password')
                .then((result) => {
                    resolve(result)
                })
                .catch((err: any) => {
                    reject(err);
                })
        })
    }
    //function to check the given email is exist
    verifyEmail = (emailId: string): Promise<IUser | null> => {
        return new Promise(function (resolve, reject) {
            User.findOne({ email: emailId }, '_id email').then((data) => {
                resolve(data);
            }).catch((err) => {
                console.log(err);
                reject(err);
            })
        })
    }

    //function to save user detials in db
    addUser = (userMdl:IUser):Promise<IUser> => {
        return new Promise(function (resolve, reject) {
            let newUser = new User(userMdl);
            newUser.save()
                .then((data) => {
                    resolve(data);
                })
                .catch((err) => {
                    console.log(err);
                    reject(err);
                })
        })
    }

     profile = (email:string):Promise<IUser|null> => {
        return new Promise(function (resolve, reject) {
            User.findOne({ email: email }, { password: 0 }).then((data) => {
                resolve(data);
            }).catch((err) => {
                console.log(err);
                reject(err);
            })
        })
    }


}


