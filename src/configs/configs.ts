import { Application } from 'express'
import startMongoDbServer from './mongodb_config'
import connect from './server_config'
import middlewares from '../middlewares/middlewares'
const startServer = (app:Application) => {
    startMongoDbServer('mongodb://localhost:27017/mydb')
    middlewares(app)
    connect(app)
}
export default startServer
