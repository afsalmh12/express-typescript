import mongoose from "mongoose";
 const startMongoDbServer = (url:string) => {
    mongoose.connect(url, { useNewUrlParser: true ,useUnifiedTopology: true});
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'DB connection error:'));
    db.once('open', function () {
        // we're connected!
        console.log("DB connection successful");
    });
}
export default startMongoDbServer