import { Request } from 'express'

export interface ExpressHttpRequest extends Request{
    _id : string
}