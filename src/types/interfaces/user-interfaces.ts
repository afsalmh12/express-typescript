import mongoose from "mongoose";
export interface mongoDBId extends  mongoose.Document{

}
export interface IAuth{
    email:string,
    password:string
}
export interface IToken{
    token : string
}

export interface IUser extends IAuth,mongoDBId{
    name: String
}